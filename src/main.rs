extern crate nom;
extern crate cse262_project;

use cse262_project::{program, run};

fn main() {
  let (unparsed, result) = program(r#"fn main() {
    return foo();
  }
  fn foo(){
    let x = 5;
    return x;
  }"#).unwrap();
  println!("{:?}", result);

  let output = run(&result);
  println!("{:?}", output);


}
